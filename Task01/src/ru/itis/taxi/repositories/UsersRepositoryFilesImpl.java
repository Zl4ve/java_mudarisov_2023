package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String userAsString;
            String[] userInfo;
            while ((userAsString = bufferedReader.readLine()) != null) {
                userInfo = userAsString.split("\\|");
                users.add(new User(UUID.fromString(userInfo[0]), userInfo[1], userInfo[2], userInfo[3], userInfo[4]));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        List<User> users = findAll();
        boolean needToUpdate = false;
        for (User user : users) {
            if (user.getId().equals(entity.getId())) {
                users.set(users.indexOf(user), entity);
                needToUpdate = true;
                break;
            }
        }

        if (needToUpdate) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))) {
                for (User user : users) {
                    String userAsString = userToString.apply(user);
                    bufferedWriter.write(userAsString);
                    bufferedWriter.newLine();
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    @Override
    public void delete(User entity) {
        deleteById(entity.getId());
    }

    @Override
    public void deleteById(UUID id) {
        List<User> users = findAll();
        boolean idFound = false;
        for (User user : users) {
            if (user.getId().toString().equals(id.toString())) {
                users.remove(user);
                idFound = true;
                break;
            }
        }

        if (idFound) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))) {
                for (User user : users) {
                    String userAsString = userToString.apply(user);
                    bufferedWriter.write(userAsString);
                    bufferedWriter.newLine();
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    @Override
    public User findById(UUID id) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String userAsString;
            String[] userInfo;
            while ((userAsString = bufferedReader.readLine()) != null) {
                userInfo = userAsString.split("\\|");
                if (userInfo[0].equals(id.toString())) {
                    return new User(UUID.fromString(userInfo[0]), userInfo[1], userInfo[2], userInfo[3], userInfo[4]);
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }
}
