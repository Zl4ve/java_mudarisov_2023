package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;


public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Ренат", "Мударисов", "mudarisov.renat@gmail.com", "12345"));
        usersService.signUp(new SignUpForm("Вася", "Пупкин", "vasya@gmail.com", "12421313"));
        usersService.signUp(new SignUpForm("Наруто", "Узумаки", "i.am.the.best.hokage@konoha.jp", "rasengan"));

        usersRepository.update(new User(usersRepository.findAll().get(0).getId(), "Ренат", "Мударисов", "mudarisov.renat@gmail.com", "NEWPASSWORD"));

        System.out.println(usersRepository.findById(usersRepository.findAll().get(0).getId()));

        User user = usersRepository.findAll().get(1);

        usersRepository.delete(user);
        usersRepository.deleteById(user.getId());

    }
}
