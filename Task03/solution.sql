-- Task 1
select pc.model as model, pc.speed as speed, pc.hd as hd
from pc pc
where pc.price < 500;

-- Task 2
select distinct p.maker as maker
from product p
where p.type = 'printer';

-- Task 3
select l.model as model, l.ram as ram, l.screen as screen
from laptop l
where l.price > 1000;

-- Task 4
select *
from printer
where color = 'y';

-- Task 5
select model, speed, hd
from pc
where (cd = '12x' or cd = '24x')
  and price < 600;

-- Task 6
select distinct p.maker as maker, l.speed as speed
from product p
         inner join laptop l on (p.model = l.model) and l.hd >= 10;

-- Task 7
select model, price
from pc
where model in (select model from product where maker = 'B')
union
select model, price
from laptop
where model in (select model from product where maker = 'B')
union
select model, price
from printer
where model in (select model from product where maker = 'B');

-- Task 8
select maker
from product
where type = 'pc'
except
select maker
from product
where type = 'laptop';

-- Task 9
select distinct maker
from product p
         inner join pc pc on (p.model = pc.model and pc.speed >= 450 and p.type = 'PC');

-- Task 10
select model, price
from printer
where price = (select max(price) from printer);

-- Task 11
select avg(speed)
from pc;

-- Task 12
select avg(speed)
from laptop
where price > 1000;

--Task 13
select avg(speed)
from pc pc
         inner join product p on (maker = 'A' and pc.model = p.model);

-- Task 15
select hd
from pc
group by hd
having count(*) >= 2;

-- Task 16
select distinct pc1.model, pc2.model, pc1.speed, pc1.ram
from pc pc1,
     pc pc2
where (pc1.speed = pc2.speed and pc1.ram = pc2.ram and pc1.model > pc2.model);

-- Task 17
select distinct type, l.model, speed
from laptop l,
     product
where speed < all (select speed from pc)
  and type = 'Laptop';

-- Task 18
select distinct maker, price
from printer
         inner join product on printer.model = product.model
where price = (select min(price) from printer where color = 'y')
  and color = 'y';

-- Task 19
select maker, avg(screen)
from product
         inner join laptop on product.model = laptop.model
group by maker;

-- Task 20
select maker, count(model)
from product
where type = 'pc'
group by maker
having count(model) >= 3;

-- Task 21
select maker, max(pc.price)
from product
         inner join pc on pc.model = product.model
group by maker;

-- Task 22
select pc1.speed, avg(pc2.price)
from pc pc1,
     pc pc2
where pc1.speed > 600
  and pc1.speed = pc2.speed
group by pc1.speed;

-- Task 23
select maker
from product
         inner join pc on product.model = pc.model
where speed >= 750
intersect
select maker
from product
         inner join laptop on product.model = laptop.model
where speed >= 750;

-- Task 24 (жесть...)
select model
from (select model, price
      from pc
      where price in (select max(price) from pc)
      union
      select model, price
      from laptop
      where price in (select max(price) from laptop)
      union
      select model, price
      from printer
      where price in (select max(price) from printer)) table1
where table1.price in (select max(price)
                       from (select model, price
                             from pc
                             where price in (select max(price) from pc)
                             union
                             select model, price
                             from laptop
                             where price in (select max(price) from laptop)
                             union
                             select model, price
                             from printer
                             where price in (select max(price) from printer)) table2);

-- Task 25
select distinct maker
from pc
         inner join product on pc.model = product.model
where maker in (select maker from product where type = 'printer')
  and ram = (select min(ram) from pc)
  and speed = (select max(speed) from pc where ram = (select min(ram) from pc));

-- Task 26
select avg(pr)
from (select price as pr
      from pc
               inner join product on pc.model = product.model
      where product.maker = 'A'
      union all
      select price as pr
      from laptop
               inner join product on laptop.model = product.model
      where product.maker = 'A') table1;

-- Task 27
select maker, avg(hd)
from product
         inner join pc on product.model = pc.model
where maker in (select maker from product where type = 'printer')
group by maker;

-- Task 28
select count(maker) as count
from (select maker from product group by maker having count (model) = 1) table1;

-- Task 29
select income_o.point, income_o.date, inc, out
from income_o
         left join outcome_o on income_o.point = outcome_o.point and income_o.date = outcome_o.date
union
select outcome_o.point, outcome_o.date, inc, out
from income_o
         right join outcome_o on income_o.point = outcome_o.point and income_o.date = outcome_o.date;

-- Task 30
select point, date, sum (out), sum (inc)
from (select point, date, out, null inc from outcome
    union all
    select point, date, null out, inc from income) table1
group by point, date;

-- Task 31
select class, country
from classes
where bore >= 16;

-- Task 32
select country, cast(avg(power(bore, 3) / 2) as numeric(6, 2)) as weight
from (
         select country, bore, name
         from classes
                  inner join ships on classes.class = ships.class
         union
         select country, bore, ship
         from classes
                  inner join outcomes on class = ship
         where ship not in (select name from ships)) table1
group by country;

-- Task 33
select ship
from outcomes
where battle = 'North Atlantic'
  and result = 'sunk';

-- Task 34
select name
from ships
where launched >= 1922
  and class in (select class from classes where displacement > 35000 and type = 'bb');

-- Task 35
select model, type
from product
where model not like '%[^0-9]%'
   or model not like '%[^a-z]%';

-- Task 36
select name
from ships
where name = class
union
select class
from classes
         join outcomes on class = ship;

-- Task 37
select class
from (select class, count(name) as name_count
      from (select classes.class, name
            from classes
                     inner join ships on classes.class = ships.class
            union
            select class, ship
            from classes
                     inner join outcomes on ship = class) table1
      group by class
      having count(*) = 1) table2;

-- Task 38
select country
from classes
where type = 'bb'
intersect
select country
from classes
where type = 'bc';

-- Task 39
select distinct ship
from outcomes o
         inner join battles b on battle = name
where result = 'damaged'
  and exists(select ship
             from outcomes
                      inner join battles on battle = name
             where ship = o.ship
               and b.date < date);

-- Task 40
select distinct maker, type
from product
where maker in (select maker from product group by maker having count(model) > 1 and count(distinct type) = 1);
