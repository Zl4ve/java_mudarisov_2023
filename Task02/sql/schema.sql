drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists "order";

create table client (
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    phone_number char(11)
);

create table driver (
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    car_id bigserial,
    experience integer check ( experience >= 3 )
);

create table car (
    id bigserial primary key,
    brand char(20),
    model char(20),
    color char(20)
);

create table "order" (
    id bigserial primary key,
    client_id bigserial,
    driver_id bigserial,
    departure_point char(50),
    arrival_point char(50),
    order_date timestamp,
    price integer
);

alter table driver add foreign key(car_id) references car(id);
alter table "order" add foreign key(client_id) references client(id);
alter table "order" add foreign key(driver_id) references driver(id);
