insert into client(first_name, last_name, phone_number)
values ('Ренат', 'Мударисов', '88005553535'),
       ('Василий', 'Абашев', '81234124124'),
       ('Виктор', 'Цой', '84231325685');

insert into driver(first_name, last_name, car_id, experience)
values ('Вадим', 'Иванов', 1, 5),
       ('Джесси', 'Пинкман', 3, 10),
       ('Леви', 'Акерман', 2, 4);

insert into car(brand, model, color)
values ('BMW', 'X5', 'Чёрный'),
       ('Lada', 'Granta', 'Белый'),
       ('Opel', 'Astra', 'Красный');

insert into "order"(client_id, driver_id, departure_point, arrival_point, order_date)
values ('1', '2', 'Деревня Универсиады, 5', 'Кремлевская, 35', '2022-05-05'),
       ('2', '3', 'ТЦ Кольцо', 'Кремлевская, 18', '2022-06-01'),
       ('3', '4', 'Ленинский мемориал', 'Тельмана, 12', '2022-05-03');

update client set phone_number = '89370390919' where id = 1;
update driver set experience = 6 where id = 2;
