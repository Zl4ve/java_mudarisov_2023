package ru.itis.repositories;

import ru.itis.models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * 08.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_STUDENTS = "select * from student;";

    //language=SQL
    private static final String SQL_SAVE_STUDENT = "insert into student(first_name, last_name, age) values (?, ?, ?);";

    //language=SQL
    private static final String SQL_GET_STUDENT_BY_ID = "select * from student where id = ?";

    private static final Function<ResultSet, Student> studentMapper = row -> {

        try {
            Long id = row.getLong("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            Integer age = row.getObject("age", Integer.class);
            return new Student(id, firstName, lastName, age);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    private final DataSource dataSource;

    public StudentsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Student> findAll() {

        List<Student> students = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_STUDENTS)) {
                while (resultSet.next()) {
                    students.add(studentMapper.apply(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return students;
    }

    @Override
    public void save(Student student) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE_STUDENT)) {

            preparedStatement.setString(1, student.getFirstName());
            preparedStatement.setString(2, student.getLastName());
            preparedStatement.setObject(3, student.getAge());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Student> findById(Long id) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_STUDENT_BY_ID)) {

            preparedStatement.setLong(1, id);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.ofNullable(studentMapper.apply(resultSet));
                }
            }

            return Optional.empty();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
